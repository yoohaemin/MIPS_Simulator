#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#define MAX_FILE_INPUT_LENGTH 50

//This should be in a header file
#define MEM_SIZE 1000
#define LABEL_NAME_SIZE 20

//We can define the max size of the label list, or we can make it a linked list
#define LABEL_LIST_SIZE 30

extern uint32_t mem[MEM_SIZE];

enum INST_TYPES {
    I_TYPE, R_TYPE, J_TYPE
};

typedef struct Label {
    int memloc;
    char name[LABEL_NAME_SIZE];
} Label;

static Label *label_list[LABEL_LIST_SIZE];

uint32_t assemble(char*, Label**);

int arg_to_num(char* args) {
    int value = 0;

    //If the argument is an immediate constant
    if ((value = strtol(args, &args, 10))) {
        return value;
    }

    //If the argument is a register
    if (!strcmp(args, "$zero")) {
        value = 0;
    } else if (!strcmp(args, "$t8")) {
        value = 24;
    } else if (!strcmp(args, "$a2")) {
        value = 6;
    } else if (!strcmp(args, "$s7")) {
        value = 23;
    } else if (!strcmp(args, "$s6")) {
        value = 22;
    } else if (!strcmp(args, "$gp")) {
        value = 28;
    } else if (!strcmp(args, "$s0")) {
        value = 16;
    } else if (!strcmp(args, "$v1")) {
        value = 3;
    } else if (!strcmp(args, "$sp")) {
        value = 29;
    } else if (!strcmp(args, "$t4")) {
        value = 12;
    } else if (!strcmp(args, "$t6")) {
        value = 14;
    } else if (!strcmp(args, "$t0")) {
        value = 8;
    } else if (!strcmp(args, "$a3")) {
        value = 7;
    } else if (!strcmp(args, "$a1")) {
        value = 5;
    } else if (!strcmp(args, "$t2")) {
        value = 10;
    } else if (!strcmp(args, "$v0")) {
        value = 2;
    } else if (!strcmp(args, "$t9")) {
        value = 25;
    } else if (!strcmp(args, "$s4")) {
        value = 20;
    } else if (!strcmp(args, "$ra")) {
        value = 31;
    } else if (!strcmp(args, "$k0")) {
        value = 26;
    } else if (!strcmp(args, "$a0")) {
        value = 4;
    } else if (!strcmp(args, "$at")) {
        value = 1;
    } else if (!strcmp(args, "$t3")) {
        value = 11;
    } else if (!strcmp(args, "$s1")) {
        value = 17;
    } else if (!strcmp(args, "$t7")) {
        value = 15;
    } else if (!strcmp(args, "$t1")) {
        value = 9;
    } else if (!strcmp(args, "$k1")) {
        value = 27;
    } else if (!strcmp(args, "$s5")) {
        value = 21;
    } else if (!strcmp(args, "$s3")) {
        value = 19;
    } else if (!strcmp(args, "$fp")) {
        value = 30;
    } else if (!strcmp(args, "$t5")) {
        value = 13;
    } else if (!strcmp(args, "$s2")) {
        value = 18;
    }
    return value;
}

//Feed assemble() source lines line-by-line and put the result to the memory
void parse_file(FILE * source) {
    int mem_count = 0;
    int label_count = 0;
    char filein_buf[MAX_FILE_INPUT_LENGTH];

    //Loop until there is no more code to read in
    while (fgets(filein_buf, MAX_FILE_INPUT_LENGTH, source)) {
        uint32_t instruction = assemble(filein_buf, &label_list[label_count]);

        //If the instruction is translated successfully into machine code
        if (instruction)
            mem[mem_count++] = instruction;

        //If the given line is a label
        else if (label_list[label_count]) {
            label_list[label_count] -> memloc = mem_count * 4;
            ++label_count;
        }
    }
}

bool is_blank(char ch) {
    if (ch == ' ' || ch == ',' || ch == '(' || ch == ')')
        return true;
    return false;
}

//Trim string(destructively) (needs to remove comment)
void trim(char** line, char** args) {
    while (is_blank(**line)) {
        ++(*line);
    }
    char* parser = *line;
    args[0] = parser;

    while (*parser != '\0') {
        if (is_blank(*parser)) {
            *parser = '\0';
        } else if (*parser == '#') {
            //If there is a comment sign, remove all of the contents
            while (*parser != '\0') {
                *parser++ = '\0';
            }
        }
        ++parser;
    }
    parser = args[0];
    int i = 1;
    while (!*parser) {
        if (parser[-1] == '\0' && parser[0] != '\0')
            args[i++] = parser;
        parser++;
    }
}

//assemble line-by-line
//new_label receives a nullptr, and malloc()s a new Label if the instruction contains a label
uint32_t assemble(char * line, Label ** new_label) {
    char * args[4] = {0, }; //string value of the arguments
    trim(&line, args);

    //If the line is a label, create a new Label and return
    if (args[0][strlen(args[0])-1] == ':') {
        *new_label = (Label*)malloc(sizeof(Label));
        strcpy(line, (*new_label) -> name);
        return 0;
    }

    uint32_t instruction = 0;

    //The handling of args depends on this value
    unsigned int inst_type;

    //Real value of the arguments, after processing(requires separate function)
    unsigned int *arg_value[3];
    for (int i = 0; i < 3; i++) {

    }

    char * opcode = args[0];

    if (!strcmp(opcode, "lui")) {
        instruction |= 0x3c000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "add")) {
        instruction |= 0x00000020;
        inst_type = R_TYPE;
    } else if (!strcmp(opcode, "sub")) {
        instruction |= 0x00000022;
        inst_type = R_TYPE;
    } else if (!strcmp(opcode, "slt")) {
        instruction |= 0x0000002a;
        inst_type = R_TYPE;
    } else if (!strcmp(opcode, "addi")) {
        instruction |= 0x20000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "slti")) {
        instruction |= 0x28000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "and")) {
        instruction |= 0x00000024;
        inst_type = R_TYPE;
    } else if (!strcmp(opcode, "or")) {
        instruction |= 0x00000025;
        inst_type = R_TYPE;
    } else if (!strcmp(opcode, "xor")) {
        instruction |= 0x00000026;
        inst_type = R_TYPE;
    } else if (!strcmp(opcode, "nor")) {
        instruction |= 0x00000027;
        inst_type = R_TYPE;
    } else if (!strcmp(opcode, "andi")) {
        instruction |= 0x30000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "ori")) {
        instruction |= 0x34000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "xori")) {
        instruction |= 0x38000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "lw")) {
        instruction |= 0x8c000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "sw")) {
        instruction |= 0xac000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "j")) {
        instruction |= 0x08000000;
        inst_type = J_TYPE;
    } else if (!strcmp(opcode, "jr")) {
        instruction |= 0x00000008;
        inst_type = R_TYPE;
    } else if (!strcmp(opcode, "bltz")) {
        instruction |= 0x04000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "beq")) {
        instruction |= 0x10000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "bne")) {
        instruction |= 0x14000000;
        inst_type = I_TYPE;
    }

    return instruction;
}
