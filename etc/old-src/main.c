#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define REG_SIZE 32
#define MEM_SIZE 1000

enum REGISTER {
    zero, at, v0, v1, a0, a1, a2, a3, t0, t1, t2, t3, t4, t5, t6, t7,
    s0, s1, s2, s3, s4, s5, s6, s7, t8, t9, k0, k1, gp, sp, fp, ra
};

uint32_t mem[MEM_SIZE];
uint32_t reg[REG_SIZE];
uint32_t pc;

//Decodes the file( using decode() ) and load the file into memory
void load_file(const char * filename) {
    //Open the file using filename
    FILE* source = fopen(filename, "r");

    //Feed the open file to parse-file (parse-file() will load the program into the memory)
    parse_file(source);

    //Close the file
    fclose(source);
}

//Sets the program counter to the desired location
void jump(int loc) {
    pc = loc;
}

//Runs the program until the end
void go() {

}

//Execute one instruction
void step() {

}

//Prints the memory
void view_memory() {
    int mem_start = strtol(args, &args, 10);
    int mem_end = strtol(args, &args, 10);
    for (int i = mem_start; i < mem_end; i++)
        printf("%d", mem[i]);
    printf("\n");
}

//Prints the register
void view_register() {
    for (int i = 0; i < REG_SIZE; i++)
        printf("%d> %d\n", i, reg[i]);
}

//Exits the program
//Must deallocate all allocated heap objects to prevent memory leak
void quit() {
    exit(EXIT_SUCCESS);
}

//Sets a specified register to a specific value
void set_register(char* args) {
    //Get the register number from the string
    int reg_no = decode_register_string(args);
    //Move args to point the value part of the arguments

    //Get the value from the string
    int reg_value = strtol(args, &args, 10);

    reg[reg_no] = reg_value;
}


//Sets the memory(Receives memory_loc and its value)
//Must be careful about endianness!!
void set_memory(int memory_loc, unsigned char value) {
    int mem_no = strtol(args, &args, 10);
    int mem_value = strtol(args, &args, 10);
    mem[mem_no] = mem_value;
}

#define MAX_STDIN_INPUT_LENGTH 100
0

int main(void) {
    char stdin_buf[MAX_STDIN_INPUT_LENGTH + 1];
    char filein_buf[MAX_FILE_INPUT_LENGTH + 1];
    FILE* file = NULL;

    while(1) {
        fgets(stdin_buf, MAX_STDIN_INPUT_LENGTH + 1, stdin);

        //remove newline character(if it exists)
        char* newline = strchr(stdin_buf, '\n');
        if (newline)
            *newline = '\0';

        //separate the string into command and argument parts
        char* args = strchr(stdin_buf, ' ');
        if (args)
            *args++ = '\0';

        switch(*stdin_buf) {
        case 'l': //Load Program
            load_file(args);
            break;
        case 'j': //Jump Program
            //
            break;
        case 'g': //Go Program
            go();
            break;
        case 's': //s has three commands(s, sr, sm)
            switch(*stdin_buf+1) {
            case '\0': //Step
                step();
                break;
            case 'r': //Set Register
                set_register(args);
                break;
            case 'm': //Set Memory
                set_memory(args);
                break;
            default:
                puts("Wrong Command, Please Retry");
            }
            break;
        case 'm':; //View Memory
            view_memory();
            break;
        case 'r': //View Register
            view_register();
            break;
        case 'x': //Exit
            quit();
            break;
        default: //Wrong Command
            puts("Wrong Command, Please Retry");
            //exit(EXIT_FAILURE);
        }
    }

    return EXIT_SUCCESS;
}
