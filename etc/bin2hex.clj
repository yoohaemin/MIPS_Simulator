(defn opcode
  ([x y]
   (let [format-2ch
         (fn [s]
           (if (= 1 (count s))
             (str "0" s) s))]
     (str (format-2ch (Integer/toHexString (bit-shift-left x 2)))
          "0000"
          (format-2ch (Integer/toHexString y))))))

(defn opcode-type
  [xs]
  (if (= (count xs) 1)
    "I_TYPE"
    "R_TYPE"))

(defn print-opcode
  [op & args]
  (println (str
            "    } else if (!strcmp(opcode, \"" op "\")) {\n        instruction |= 0x"
            (opcode
             (first args)
             (if (= (count args) 2)
               (last args) 0)) \;"\n        inst_type = "(opcode-type args)\;)))

(print-opcode "lui" 15)
(print-opcode "add" 0 32)
(print-opcode "sub" 0 34)
(print-opcode "slt" 0 42)
(print-opcode "addi" 8)
(print-opcode "slti" 10)
(print-opcode "and" 0 36)
(print-opcode "or" 0 37)
(print-opcode "xor" 0 38)
(print-opcode "nor" 0 39)
(print-opcode "andi" 12)
(print-opcode "ori" 13)
(print-opcode "xori" 14)
(print-opcode "lw" 35)
(print-opcode "sw" 43)
(print-opcode "j" 2)
(print-opcode "jr" 0 8)
(print-opcode "bltz" 1)
(print-opcode "beq" 4)
(print-opcode "bne" 5)

;;Results:
(comment "
    } else if (!strcmp(opcode, "lui")) {
        instruction |= 0x3c000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "add")) {
        instruction |= 0x00000020;
        inst_type = R_TYPE;
    } else if (!strcmp(opcode, "sub")) {
        instruction |= 0x00000022;
        inst_type = R_TYPE;
    } else if (!strcmp(opcode, "slt")) {
        instruction |= 0x0000002a;
        inst_type = R_TYPE;
    } else if (!strcmp(opcode, "addi")) {
        instruction |= 0x20000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "slti")) {
        instruction |= 0x28000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "and")) {
        instruction |= 0x00000024;
        inst_type = R_TYPE;
    } else if (!strcmp(opcode, "or")) {
        instruction |= 0x00000025;
        inst_type = R_TYPE;
    } else if (!strcmp(opcode, "xor")) {
        instruction |= 0x00000026;
        inst_type = R_TYPE;
    } else if (!strcmp(opcode, "nor")) {
        instruction |= 0x00000027;
        inst_type = R_TYPE;
    } else if (!strcmp(opcode, "andi")) {
        instruction |= 0x30000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "ori")) {
        instruction |= 0x34000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "xori")) {
        instruction |= 0x38000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "lw")) {
        instruction |= 0x8c000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "sw")) {
        instruction |= 0xac000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "j")) {
        instruction |= 0x08000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "jr")) {
        instruction |= 0x00000008;
        inst_type = R_TYPE;
    } else if (!strcmp(opcode, "bltz")) {
        instruction |= 0x04000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "beq")) {
        instruction |= 0x10000000;
        inst_type = I_TYPE;
    } else if (!strcmp(opcode, "bne")) {
        instruction |= 0x14000000;
        inst_type = I_TYPE;
")


"lui" 0x3c000000
"add" 0x00000020
"sub" 0x00000022
"slt" 0x0000002a
"addi" 0x20000000
"slti" 0x28000000
"and" 0x00000024
"or" 0x00000025
"xor" 0x00000026
"nor" 0x00000027
"andi" 0x30000000
"ori" 0x34000000
"xori" 0x38000000
"lw" 0x8c000000
"sw" 0xac000000
"j" 0x08000000
"jr" 0x00000008
"bltz" 0x04000000
"beq" 0x10000000
"bne" 0x14000000
