(defproject machine-code-gen "0.1.0-SNAPSHOT"
  :description "A MIPS machine code generator from MIPS Assembly files."
  :url ""
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :main ^:skip-aot machine-code-gen.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
