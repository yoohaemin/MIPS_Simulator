main:
        # Allocate 16 bytes at the stack
	addi	$sp,$sp,-16
	sw	$fp,12($sp)
	add	$fp,$sp,$0

        # int i = 0;
	sw	$0,0($fp)

        # int j = i + 1;
	lw	$2,0($fp)
	addi	$2,$2,1
	sw	$2,4($fp)

        # if (!i)
	lw	$2,0($fp)
	bne	$2,$0,$L2       # Branch if not equal to zero

        # i == 0
	lw	$2,4($fp)
	addi	$2,$2,1
	sw	$2,4($fp)
$L2:
	add	$2,$0,$0
	add	$sp,$fp,$0
	lw	$fp,12($sp)
	addi	$sp,$sp,16
	jr	$31
