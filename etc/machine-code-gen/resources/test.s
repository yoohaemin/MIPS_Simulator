# mips-linux-gnu-gcc -mips1 -mips16 -mfp32 -mgp32 -S -O0 test.c -o test2.s

	.file	1 "test.c"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.module	fp=32
	.module	nooddspreg
	.abicalls
	.text
	.align	2
	.globl	main
	.set	mips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$17,24,$31		# vars= 8, regs= 1/0, args= 0, gp= 8
	.mask	0x00020000,-4
	.fmask	0x00000000,0
	addiu	$sp,-24
	sw	$17,20($sp)
	move	$17,$sp
	li	$2,0
	sw	$2,8($17)
	lw	$2,8($17)
	addiu	$2,1
	sw	$2,12($17)
	lw	$2,8($17)
	beqz	$2,$L2
	lw	$2,12($17)
	addiu	$2,1
	sw	$2,12($17)
$L2:
	li	$2,0
	move	$sp,$17
	lw	$17,20($sp)
	addiu	$sp,24
	jr	$31
	.end	main
	.size	main, .-main
	.ident	"GCC: (Debian 6.1.1-9) 6.1.1 20160705"
