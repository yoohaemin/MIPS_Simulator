(defn read-assembly
  "takes an assembly file(as string), and returns a vector of 32-bit ints, which then can be directly written to the desired output file."
  [s]
  (letfn
      [(iter
         [s addr mem-acc label-acc]
         (if (empty? s) mem-acc) ;returns mem if there is nothing more to read
         (if (= (last (this-line s)) \:)  ;if the given line is a label
           (recur
            (next-line s)
            addr
            mem-acc
            (set-label (this-line s) addr label-acc))
           (recur ;if the given line is not a label
            (next-line s)
            (+ 4 addr)
            (conj mem-acc (convert-single-instr (this-line s)))
            label-acc)))]
    (iter s 0 [] {})))
