(ns machine-code-gen.core
  (:gen-class))

"""
this program is a basic assembler for outputting binary files, given an assmbly file.
this program only supports labels, instructions, and a few frequently used pseudoinstructions.
the assembly code should be formatted nicely, with either a single label or a single instruction in a given line.
"""

(def opcodes
  (array-map
   "lui"  [0x3c000000 :rt :imm]
   "add"  [0x00000020 :rd :rs :rt]
   "sub"  [0x00000022 :rd :rs :rt]
   "slt"  [0x0000002a :rd :rs :rt]
   "addi" [0x20000000 :rd :rs :imm]
   "slti" [0x28000000 :rd :rs :imm]
   "and"  [0x00000024 :rd :rs :rt]
   "or"   [0x00000025 :rd :rs :rt]
   "xor"  [0x00000026 :rd :rs :rt]
   "nor"  [0x00000027 :rd :rs :rt]
   "andi" [0x30000000 :rd :rs :imm]
   "ori"  [0x34000000 :rd :rs :imm]
   "xori" [0x38000000 :rd :rs :imm]
   "lw"   [0x8c000000 :rt :imm :rs]
   "sw"   [0xac000000 :rt :imm :rs]
   "j"    [0x08000000 :l]
   "jr"   [0x00000008 :rs]
   "bltz" [0x04000000 :rs :l]
   "beq"  [0x10000000 :rs :rt :l]
   "bne"  [0x14000000 :l]))

(def shifter
  {:rt #(bit-shift-left (bit-and % 31) 16)
   :rd #(bit-shift-left (bit-and % 31) 11)
   :rs #(bit-shift-left (bit-and % 31) 21)
   :imm #(bit-and % 0x0000ffff)
   :l #(bit-and % 0x03ffffff)})

(defn next-line
  "Drops one line from a string"
  [s]
  (apply str (rest (drop-while #(not= \newline %) s))))

(defn this-line
  "Returns characters until a newline character is met."
  [s]
  (apply str (take-while #(not= \newline %) s)))

(defn split-instr-string
  "Splits the assembly instruction into words.
  Dolar signs and parentheses are treated as whitespace and ignored."
  [s]
  (filter #(not (empty? %))
          (clojure.string/split
           (clojure.string/trim s)
           #"[\$ \(\)\,\t]")))

(defn convert-single-instr
  "takes an assembly line containing a single instruction and returns a machine code representation as unchecked int(32-bit integer)"
  [assembly-str]
  (let
      [args         (split-instr-string assembly-str)
       args-num     (map #(Integer/parseInt %) (rest args))
       base         (first (get opcodes (first args)))
       code-val-map (zipmap (rest (get opcodes (first args))) args-num)]
    (unchecked-int
     (reduce-kv
      (fn [init k v]
        (bit-or
         init
         ((get shifter k) v)))
      base code-val-map))))

(defn label?
  "Returns true if the given line is a label, false if is not"
  [s]
  (= (last (clojure.string/trim s)) \:))

(defn get-label-loc
  "Removes all labels from the file (given as a vector of strings) and returns a list of
  A) The changed file (as a vector of strings)
  B) A map of all labels (as string) and all corresponding memory locations"
  [assembly-vs]
  (let
      [get-m (fn
               [vs mem-loc m-acc]
               (if (label? (clojure.string/trim (first vs)))
                 (recur
                  (rest vs)                   ;next line
                  mem-loc                     ;current memory location
                  (assoc m-acc (clojure.string/trim (first vs)) mem-loc))
                                        ;assoc label with this mem-loc
                 (recur
                  (rest vs)                   ;next line
                  (+ 4 mem-loc)               ;next memory location
                  m-acc)))                    ;same map
       label-map (get-m assembly-vs 0 {})
       str-without-label (filter #(not label?) assembly-vs)]
    (map
     (fn [s]
       ())
     str-without-label)))



;;The original assembly file with directives and comments removed
(def assembly-file-str-orig
  (filter
   #(and (not= (first %) \.) (not (empty? %)))
   (map #(clojure.string/replace (clojure.string/trim %) #"#.*" "")
        (clojure.string/split-lines (slurp "./resources/test3.s")))))

;;The label and address map
(def label-addr-map
  (into
   (array-map)
   (map #(vector (clojure.string/replace (first %) ":" "") (str (* 4 (last %))))
        (keep-indexed
         #(vector (first %2) (- (last %2) %1))
         (keep-indexed
          #(if (label? %2) (vector %2 %1)) assembly-file-str-orig)))))

(print-str (str label-addr-map))

;;The assembly fifle with labels removed
(def assembly-file-str
  (filter #(not (label? %)) assembly-file-str-orig))

;;
(def assembly-str-final
  (map #(-> %
            (clojure.string/replace "sp" "29")
            (clojure.string/replace "fp" "30"))
       (map
        (fn [s]
          (reduce-kv #(clojure.string/replace %1 %2 %3) s label-addr-map))
        assembly-file-str)))

;;(print assembly-str-final)

(print (map convert-single-instr assembly-str-final))
(->> assembly-str-final
     (map convert-single-instr)
     (map (partial format "%08x"))
     print)



(defn -main
  ""
  [& args]
  (slurp (first args)))
