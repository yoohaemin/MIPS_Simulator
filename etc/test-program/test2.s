main:
	addiu	$sp,-32
	sw	$17,28($sp)
	move	$17,$sp
	li	$2,0
	sw	$2,8($17)
	li	$2,0
	sw	$2,12($17)
	li	$2,30
	sh	$2,16($17)
	lw	$3,8($17)
	lw	$2,12($17)
	addu	$3,$3,$2
	lh	$2,16($17)
	addu	$2,$3,$2
	sw	$2,20($17)
	lw	$2,20($17)
	bnez	$2,$L2
	li	$2,42
	sh	$2,16($17)
$L2:
	li	$2,0
	move	$sp,$17
	lw	$17,28($sp)
	addiu	$sp,32
	jr	$31
