main:
	addi	$sp,$sp,-16     # Decrease $sp by 16
	sw	$fp,12($sp)     # Store the contents of $fp at 12($sp)
        add     $fp,$sp,$zero   # Set $fp equal to $sp
	sw	$0,0($fp)       # Set memory at 0($fp) to 0     => int i = 0
	lw	$2,0($fp)       # Load memory at $fp to $2
	addi	$2,$2,1         # Add 1 to $2                   => i + 1
	sw	$2,4($fp)       # Set memory at 4($fp) to $2    => j = i + 1
	lw	$2,0($fp)       # Load the contents of i to $2  => i
	beqzc	$2,$L2          # Compare i to 
	lw	$2,4($fp)       #
	addi	$2,$2,1         #
	sw	$2,4($fp)       #
$L2:
	addi	$2,$0,$0        #
	addi	$sp,$fp,$0      #
	lw	$fp,12($sp)     #
	jraddiusp	16      #
