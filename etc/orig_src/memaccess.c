#include <stdio.h>
#include "memaccess.h"

extern const int REG_SIZE;
extern const int MEM_SIZE;
extern const char *ERROR_WRONG_INPUT;

void print_reg(uint32_t reg[]){
    int i;

    for (i = 0; i < REG_SIZE; i++){

        printf("reg[%2d] = 0x%08x \n", i, reg[i]);

    }
    return;
}

void print_mem(uint8_t mem[], int start, int end){
    int i;

    for (i = start; i <= end; i++){

        printf("mem[%03d] = 0x%02x \n", i, mem[i]);

    }
    return;
}

void setm(uint8_t mem[], int pos, uint8_t data){
    if (pos >= MEM_SIZE){
        puts(ERROR_WRONG_INPUT);
        return;
    }

    mem[pos] = data;

    return;
}

void setr(uint32_t reg[], int reg_no, uint32_t data){
    if (reg_no >= REG_SIZE){
        puts(ERROR_WRONG_INPUT);
        return;
    }

    reg[reg_no] = data;

    return;

}
