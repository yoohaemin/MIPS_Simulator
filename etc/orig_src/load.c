#include <stdlib.h>

#include "load.h" //includes stdio.h, stdint.h

extern const int MEM_SIZE;

static const char *ERROR_EMPTY_FILE = "ERROR: Empty file";

void load_file(uint8_t *mem, FILE *bin_f) {
    size_t file_size = fread(mem, sizeof(uint8_t), MEM_SIZE * 4, bin_f);
    if (!file_size) //This probably shouldn't happen
        puts(ERROR_EMPTY_FILE);
}
