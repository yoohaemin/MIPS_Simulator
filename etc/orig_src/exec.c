#include <stdio.h>
#include <stdlib.h>
#include "exec.h" //Also includes <stdint.h>
/*
void go(uint8_t mem[], uint32_t reg[], int* pc);
void step(uint8_t mem[], uint32_t reg[], int* pc); //수정:mem[]배열의 타입을 uint32_t에서 uint8_t로 변경함.
*/
//헤더파일에 있는거랑 중복선언이라 지웠습니다!!

static const char* ERROR_BAD_INSTR = "ERROR: Bad Instruction";
extern const char* ERROR_BAD_MEMACCESS;

uint32_t changeEndian(uint32_t src);

//지역변수와 인자의 첫번째 문자는 소문자로 하는것이 관행입니다
//Instruction -> instruction
//첫번째 문자를 대문자로 하는건 주로 클래스 이름입니다.
uint8_t getRs(uint32_t instruction);
uint8_t getRt(uint32_t instruction);
uint8_t getRd(uint32_t instruction);
uint8_t getFn(uint32_t instruction);

uint16_t getImmediateValue(uint32_t instruction);
uint32_t getJumpTargetAddr(uint32_t instruction, int pc);

uint32_t alu(int fn, uint32_t v1, uint32_t v2);

//인자 목록이 길 경우에는 엔터로 띄워주는게 좋습니다
//하나의 함수 안에 인자는 4개를 넘지 않는 것이 권장됩니다.
void aluImmediate(int opcode,
                   uint8_t rs,
                   uint8_t rt,
                   uint16_t immediateValue,
                   uint32_t reg[],
                   int *pc);

void lsImmediate(int opcode,
                  uint8_t rs,
                  uint8_t rt,
                  uint16_t immediateValue,
                  uint32_t reg[],
                  uint8_t mem[],
                  int *pc); //Load and Store

void branchImmediate(int opcode,
                      uint8_t rs,
                      uint8_t rt,
                      uint16_t immediateValue,
                      uint32_t reg[],
                      int *pc); //Branch

uint32_t getWord(uint8_t mem[], int loc) {
    if (loc % 4) {
        fputs(ERROR_BAD_MEMACCESS, stderr);
        fputs("\n", stderr);
        exit(EXIT_FAILURE);
    }
    return changeEndian(*(uint32_t*)&mem[loc]);
}


void go(uint8_t mem[], uint32_t reg[], int* pc)
{
    while(step(mem, reg, pc))
        ;
}

//Returns true if the processed instruction is not syscall, false if otherwise.
bool step(uint8_t mem[], uint32_t reg[], int* pc) //수정:mem[]배열의 타입을 uint32_t에서 uint8_t로 변경함.
{
    uint8_t op, rs, rt, rd, fn;
    uint16_t immediateValue; //이름을 immVal로 했으면 어땠을까 해요
    uint32_t instruction = getWord(mem, *pc);

    //포인터 타입때문에 8바이트밖에 못읽어요 ㅠㅠ
    //instruction = mem[*pc];

    //뒷문장은 굳이 필요없어요(unsigned 정수라서 어차피 앞자리는 0으로 채워져요)
    op = instruction >> 26;
    //op = 0x0000003F & op;

    printf("opcode: %u\n", op);

    switch (op)
        {
        case 0: //R type instruction
            rs = getRs(instruction);
            rt = getRt(instruction);
            rd = getRd(instruction);
            fn = getFn(instruction);
            switch (fn)
                {
                case 8:
                    break;//수정:switch~case에는 항상 case마다 break;있어야 함. 그렇지 않으면 break가 나올 때까지 쭉 실행함.
                case 12: //syscall
                    return false;
                    break;
                default: //수정:설미란씨 코드에 각 fn마다 돌아가게 되어 있었는데 아래와 같이 두 줄로 줄임.
                    //수정:단, ALU연산이 아닌 나머지 R type 연산들은 default:문 위에 모두 기술되어 있어야 함.
                    reg[rd] = alu(fn, reg[rs], reg[rt]);
                    *pc += 4;
                    break;
                }
            break;

        case 3: //jal
            reg[31] = (uint32_t)(*pc + 8);
            //fallthrough

        case 2: //j
            *pc = getJumpTargetAddr(instruction, *pc);
            break;

        default: //I type instruction
            rs = getRs(instruction);
            rt = getRt(instruction);
            immediateValue = getImmediateValue(instruction);
            switch (op)
                {
                case 1: //btlz
                    branchImmediate(op, rs, rt, immediateValue, reg, pc);
                    break;

                case 4: //beq
                    branchImmediate(op, rs, rt, immediateValue, reg, pc);
                    break;

                case 5: //bne
                    branchImmediate(op, rs, rt, immediateValue, reg, pc);
                    break;

                case 8: //addi
                    aluImmediate(op, rs, rt, immediateValue, reg, pc);
                    break;

                case 12: //andi
                    aluImmediate(op, rs, rt, immediateValue, reg, pc);
                    break;

                case 13: //ori
                    aluImmediate(op, rs, rt, immediateValue, reg, pc);
                    break;

                case 14: //xori
                    aluImmediate(op, rs, rt, immediateValue, reg, pc);
                    break;

                case 10: //slti
                    if (reg[rs] < immediateValue)
                        reg[rt] = 1;
                    else
                        reg[rt] = 0;
                    break;

                case 15: //lui
                    lsImmediate(op, rs, rt, immediateValue, reg, mem, pc);
                    break;

                case 35: //lw
                    lsImmediate(op, rs, rt, immediateValue, reg, mem, pc);
                    break;

                case 43: //sw
                    lsImmediate(op, rs, rt, immediateValue, reg, mem, pc);
                    break;

                default:
                    //에러처리를 하는게 좋아요
                    fputs(ERROR_BAD_INSTR, stderr);
                    break;
                }
        }
    return true;
}

uint8_t getRs(uint32_t instruction)
{
    uint8_t rs;
    rs = instruction >> 21;
    rs = 0x0000001F & rs;
    return rs;
}

uint8_t getRt(uint32_t instruction)
{
    uint8_t rt;
    rt = instruction >> 16;
    rt = 0x0000001F & rt;
    return rt;
}

uint8_t getRd(uint32_t instruction)
{
    uint8_t rd;
    rd = instruction >> 11;
    rd = 0x0000001F & rd;
    return rd;
}

uint8_t getFn(uint32_t instruction)
{
    uint8_t fn;
    fn = 0x0000003F & instruction;
    return fn;
}

uint16_t getImmediateValue(uint32_t instruction)
{
    //Only the significant 16 bits are truncated, as specified in the C standard.
    return (uint16_t)instruction;
}

uint32_t getJumpTargetAddr(uint32_t instruction, int pc)
{
    return (uint32_t)((0x03FFFFFF & instruction) << 2) & (pc & 0xf0000000);
}

uint32_t alu(int fn, uint32_t v1, uint32_t v2)
{
    if (fn == 32) { //add
        return v1 + v2;
    }
    else if (fn == 34) { //sub
        return v1 - v2;
    }
    else if (fn == 36) { //and
        return v1 & v2;
    }
    else if (fn == 37) { //or
        return v1 | v2;
    }
    else if (fn == 38) { //xor
        return v1 ^ v2;
    }
    else if (fn == 39) { //nor
        return !(v1 | v2);
    }
    else {
        if (v1 < v2)
            return 1;
        else
            return 0;
    }
}

void aluImmediate(int opcode,
                  uint8_t rs,
                  uint8_t rt,
                  uint16_t immediateValue,
                  uint32_t reg[],
                  int *pc)
{
    if (opcode == 8) { //addi
        reg[rt] = reg[rs] + immediateValue;
    }
    else if (opcode == 12) { //andi
        reg[rt] = reg[rs] & immediateValue;
    }
    else if (opcode == 13) { //ori
        reg[rt] = reg[rs] | immediateValue;
    }
    else if (opcode == 14) { //xori
        reg[rt] = reg[rs] ^ immediateValue;
    }
    *pc += 4;
    return;
}

void lsImmediate(int opcode,
                 uint8_t rs,
                 uint8_t rt,
                 uint16_t immediateValue,
                 uint32_t reg[],
                 uint8_t mem[],
                 int *pc) //Load and Store
{
    if (opcode == 15) { //lui
        /* 어차피 왼쪽으로 쉬프트하면 오른쪽은 0으로
           채워져서 굳이 두줄로 할필요 없어요

        luiValue = immediateValue << 16;
        reg[rt] = luiValue & 0xFFFF0000; */

        reg[rt] = immediateValue << 16;
    }
    else if (opcode == 35) { //lw
        reg[rt] = getWord(mem, reg[rs] + immediateValue);
        //lw $t0, 40($s3)
        //mem[40+($s3)]을 $t0에 load.
        //“($s3)”은 “content of $s3”을 의미한다.
    }
    else if (opcode == 43) { //sw
        uint32_t *writeptr = (uint32_t*)&mem[reg[rs] + immediateValue];
        if (!((uint64_t)writeptr / 4)) {
            fputs(ERROR_BAD_MEMACCESS, stderr);
            exit(EXIT_FAILURE);
        }
        *writeptr = changeEndian(reg[rt]);
        //12) sw $t0,A($s3)
        //($t0)를 mem[A+($s3)]에 store.
    }
    *pc += 4;
    return;
}

void branchImmediate(int opcode,
                     uint8_t rs,
                     uint8_t rt,
                     uint16_t immediateValue,
                     uint32_t reg[],
                     int *pc) //Branch
{
    if (opcode == 1) { //bltz
        if (reg[rs] >> 31) //If the most significant bit is 1
            *pc += 4 * immediateValue;
        else
            *pc += 4;
    }
    else if (opcode == 4) { //beq
        if (reg[rs] == reg[rt])
            *pc += 4 * immediateValue;
        else
            *pc += 4;
    }
    else if (opcode == 5) { //bne
        if (reg[rs] != reg[rt])
            *pc += 4 * immediateValue;
        else
            *pc += 4;
    }
    return;
}

uint32_t changeEndian(uint32_t src) {
    return (src & 0x000000FF) << 24
         | (src & 0x0000FF00) << 8
         | (src & 0x00FF0000) >> 8
         | (src & 0xFF000000) >> 24;
}
