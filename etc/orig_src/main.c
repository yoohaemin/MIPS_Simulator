#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "load.h"
#include "memaccess.h"
#include "exec.h"

/*
  Why const int, not #define?
  global const ints can be referred to in other files as extern.
  #defines cannot be referred to in such way.

  The implementation files of the functions should include the constants, as needed.
*/
const int REG_SIZE = 32;

//Memory size in bytes.
const int MEM_SIZE = 4000;

static const int  STDIN_BUF_SIZE = 100;

static const char *GREETING = "The Glorious Team 1 MIPS Simulation System.";
static const char *CREDIT = "(c)2016 Team 1";
static const char *HELP = "Press x to exit.";

const char *ERROR_OPEN_FILE = "ERROR: Cannot open file";
const char *ERROR_WRONG_INPUT = "ERROR: Wrong input";
const char *ERROR_BAD_MEMACCESS = "ERROR: Cannot access memory at that location";
const char *EXEC_FINISHED = "Execution terminated";

int main(int argc, char *argv[]) {
    FILE *bin_f = NULL;
    char input_buf[STDIN_BUF_SIZE];
    const char* command = input_buf;

    //Having a huge memory array on the stack may blow the stack.
    //Better to allocate at the heap.
    //Global variables -> Only for constants.
    uint8_t *mem = (uint8_t*)malloc(sizeof(uint8_t) * MEM_SIZE);
    uint32_t *reg = (uint32_t*)malloc(sizeof(uint32_t) * REG_SIZE);
    int pc = 0;

    //Commands from the console have at most 2 arguments
    char *stdin_args[2];

    puts(GREETING);
    puts(CREDIT);
    puts(HELP);
    puts("");
    while (true) {
        printf("> ");
        fgets(input_buf, sizeof(input_buf), stdin);

        //Trims the input
        char *newline = strchr(input_buf, '\r'); //Newline is represented as \r\n in Windows
        if (newline)
            *newline = '\0';

        //In linux:
        newline = strchr(input_buf, '\n');
        if (newline)
            *newline = '\0';


        stdin_args[0] = strchr(input_buf, ' ');
        if (stdin_args[0]) {
            *stdin_args[0]++ = '\0';

            stdin_args[1] = strchr(stdin_args[0], ' ');
            if (stdin_args[1])
                *stdin_args[1]++ = '\0';
        }

        switch (command[0]) {
        case 'l': //load file
            bin_f = fopen(stdin_args[0], "rb");
            if (bin_f) {
                load_file(mem, bin_f);
                if (fclose(bin_f)) //fclose returns 0 on success
                    exit(EXIT_FAILURE);//do something else?
            }
            else {
                fputs(ERROR_OPEN_FILE, stderr);
                fputs("\n", stderr);
            }
            break;
        case 'j': //jump
            pc = strtol(stdin_args[0], NULL, 0);
            break;
        case 'g': //go
            go(mem, reg, &pc);
            puts(EXEC_FINISHED);
            break;
        case 's':
            switch (command[1]) {
            case '\0': //step
                if (!step(mem, reg, &pc)) {
                    puts(EXEC_FINISHED);
                }
                break;
            case 'r': //set register
                setr(reg,
                     strtol(stdin_args[0], NULL, 0),
                     strtoul(stdin_args[1], NULL, 0));
                break;
            case 'm': //set memory
                setm(mem,
                     strtol(stdin_args[0], NULL, 0),
                     strtoul(stdin_args[1], NULL, 0));
                break;
            }
            break;
        case 'm': //view memory
            print_mem(mem,
                      strtol(stdin_args[0], NULL, 0),
                      strtol(stdin_args[1], NULL, 0));
            break;
        case 'r': //view register
            print_reg(reg);
            break;
        case 'x': //program exit
            free(mem);
            free(reg);
            exit(EXIT_SUCCESS);
            break;
        default: //wrong input
            fputs(ERROR_WRONG_INPUT, stderr);
            fprintf(stderr, "%c\n", input_buf[0]);
        }
        //prepare variables for next use
        input_buf[0] = '\0'; //we don't really have to clear the entire array
        stdin_args[0] = NULL;
        stdin_args[1] = NULL;
        bin_f = NULL;
    }
    return 0;
}
