#ifndef EXEC_H
#define EXEC_H

#include <stdint.h>
#include <stdbool.h>
/*
 * Executes until the end of the program(syscall).
 * Updates PC based on executed instructions.
 */
void go(uint8_t mem[], uint32_t reg[], int* pc);

/*
 * Executes one line.
 * Updates PC based on executed instructions.
 */
bool step(uint8_t mem[], uint32_t reg[], int* pc);
#endif
