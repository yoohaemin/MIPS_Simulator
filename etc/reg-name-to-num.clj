(def reg-names
  {:zero 0, :at 1, :v0 2, :v1 3, :a0 4, :a1 5, :a2 6,
   :a3 7, :t0 8, :t1 9, :t2 10, :t3 11, :t4 12,
   :t5 13, :t6 14, :t7 15, :s0 16, :s1 17, :s2 18,
   :s3 19, :s4 20, :s5 21, :s6 22, :s7 23, :t8 24,
   :t9 25, :k0 26, :k1 27, :gp 28, :sp 29, :fp 30, :ra 31})

(defn print-reg-number-function
  [m]
  (println (str
            "    } else if (!strcmp(reg_name, \"$" (apply str (rest (str m))) "\")) {
        reg_value = " (m reg-names) \;)))

(map print-reg-number-function (keys reg-names))

(type ())

;;Results:
(comment "
    } else if (!strcmp(reg_name, "zero")) {
        reg_value = 0;
    } else if (!strcmp(reg_name, "t8")) {
        reg_value = 24;
    } else if (!strcmp(reg_name, "a2")) {
        reg_value = 6;
    } else if (!strcmp(reg_name, "s7")) {
        reg_value = 23;
    } else if (!strcmp(reg_name, "s6")) {
        reg_value = 22;
    } else if (!strcmp(reg_name, "gp")) {
        reg_value = 28;
    } else if (!strcmp(reg_name, "s0")) {
        reg_value = 16;
    } else if (!strcmp(reg_name, "v1")) {
        reg_value = 3;
    } else if (!strcmp(reg_name, "sp")) {
        reg_value = 29;
    } else if (!strcmp(reg_name, "t4")) {
        reg_value = 12;
    } else if (!strcmp(reg_name, "t6")) {
        reg_value = 14;
    } else if (!strcmp(reg_name, "t0")) {
        reg_value = 8;
    } else if (!strcmp(reg_name, "a3")) {
        reg_value = 7;
    } else if (!strcmp(reg_name, "a1")) {
        reg_value = 5;
    } else if (!strcmp(reg_name, "t2")) {
        reg_value = 10;
    } else if (!strcmp(reg_name, "v0")) {
        reg_value = 2;
    } else if (!strcmp(reg_name, "t9")) {
        reg_value = 25;
    } else if (!strcmp(reg_name, "s4")) {
        reg_value = 20;
    } else if (!strcmp(reg_name, "ra")) {
        reg_value = 31;
    } else if (!strcmp(reg_name, "k0")) {
        reg_value = 26;
    } else if (!strcmp(reg_name, "a0")) {
        reg_value = 4;
    } else if (!strcmp(reg_name, "at")) {
        reg_value = 1;
    } else if (!strcmp(reg_name, "t3")) {
        reg_value = 11;
    } else if (!strcmp(reg_name, "s1")) {
        reg_value = 17;
    } else if (!strcmp(reg_name, "t7")) {
        reg_value = 15;
    } else if (!strcmp(reg_name, "t1")) {
        reg_value = 9;
    } else if (!strcmp(reg_name, "k1")) {
        reg_value = 27;
    } else if (!strcmp(reg_name, "s5")) {
        reg_value = 21;
    } else if (!strcmp(reg_name, "s3")) {
        reg_value = 19;
    } else if (!strcmp(reg_name, "fp")) {
        reg_value = 30;
    } else if (!strcmp(reg_name, "t5")) {
        reg_value = 13;
    } else if (!strcmp(reg_name, "s2")) {
        reg_value = 18;
")
