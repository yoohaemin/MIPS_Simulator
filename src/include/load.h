#ifndef LOAD_H
#define LOAD_H
#include <stdio.h>
#include <stdint.h>

/*
 * Loads the given file to the uint32_t array mem.
 * The behavior is undefined if bin_f is NULL(Check it before passing arguments!)
 * This function reverses endianness - the binary file is big-endian,
 * while the array is little-endian.
 *
 * Implement using fread()
 */
void load_file(uint8_t *mem, FILE *bin_f);

#endif
