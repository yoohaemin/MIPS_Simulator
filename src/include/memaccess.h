#ifndef MEMACCESS_H
#define MEMACCESS_H

#include <stdint.h> //for uint32_t, uint8_t

/*
  Receives an array of registers(the size is referred to as constant in the main file) and prints out registers in order.

 To the implementer: The implementation file must include REG_SIZE.
 */
void print_reg(uint32_t reg[]);

/*
  Prints out the memory at the given area.

  To the implementer: mind the endianness.
 */
void print_mem(uint8_t mem[], int start, int end);


/*
  Sets the memory at the given area.

  To the implementer:
  1) mind the endianness when accessing data.
  2) The data is only 8 bytes.(uint8_t)
 */
void setm(uint8_t mem[], int pos, uint8_t data);

/*
  Sets the given register to the given value.
 */
void setr(uint32_t reg[], int reg_no, uint32_t data);

#endif
