	addi    $s0,$zero,9
	sub     $t0,$s0,$s0
	add     $t1,$zero,$zero
test:
	bne     $t0,$s0,done
	addi    $t0,$t0,1
	add     $t1,$s0,$zero
	j       test
done:
	sw      $t1,0($sp)
