	addi	$sp,$sp,-16
	sw	$fp,12($sp)
	add	$fp,$sp,$0
        sub     $fp,$sp,$0
	sw	$0,0($fp)
	lw	$2,0($fp)
	addi	$2,$2,1
	sw	$2,4($fp)
	lw	$2,0($fp)
	lw	$2,4($fp)
	addi	$2,$2,1
	sw	$2,4($fp)
	add	$2,$0,$0
	add	$sp,$fp,$0
	lw	$fp,12($sp)
	addi	$sp,$sp,16
        lui     $t8,0xFFFF
        slt     $t6,$t7,$t8
        slti    $t7,$t6,0
        and     $t8,$t8,$zero
        or      $t6,$t6,$zero
        xor     $t6,$t6,$zero
        nor     $t6,$t6,$zero
        andi    $t6,$t6,0xFFFF
        xori    $t6,$t6,0xFFFF
        lui     $t6,0xFFFF
        ori     $t6,$t6,0xFFFE
        addi    $t6,$t6,0x0001
        jal     $L2
        addi    $t6,$t6,0x0001
$END:
        syscall
$L2:
        jr      $ra
