#include <stdio.h>
#include <stdlib.h>
#include "include/exec.h" //Also includes <stdint.h>

#include "include/debug.h"

static const char* ERROR_BAD_INSTR = "ERROR: Bad Instruction";
extern const char* ERROR_BAD_MEMACCESS;

uint32_t changeEndian(uint32_t src);

uint8_t getRs(uint32_t instruction);
uint8_t getRt(uint32_t instruction);
uint8_t getRd(uint32_t instruction);
uint8_t getFn(uint32_t instruction);

uint16_t getImmediateValue(uint32_t instruction);
uint32_t getJumpTargetAddr(uint32_t instruction, int pc);

uint32_t alu(int fn, uint32_t v1, uint32_t v2);

void aluImmediate(int opcode,
                   uint8_t rs,
                   uint8_t rt,
                   uint16_t immediateValue,
                   uint32_t reg[],
                   int *pc);

void lsImmediate(int opcode,
                  uint8_t rs,
                  uint8_t rt,
                  uint16_t immediateValue,
                  uint32_t reg[],
                  uint8_t mem[],
                  int *pc); //Load and Store

void branchImmediate(int opcode,
                      uint8_t rs,
                      uint8_t rt,
                      uint16_t immediateValue,
                      uint32_t reg[],
                      int *pc); //Branch

uint32_t getWord(uint8_t mem[], int loc) {
    if (loc % 4) {
        fputs(ERROR_BAD_MEMACCESS, stderr);
        fputs("\n", stderr);
        exit(EXIT_FAILURE);
    }
    return changeEndian(*(uint32_t*)&mem[loc]);
}


void go(uint8_t mem[], uint32_t reg[], int* pc)
{
    while(step(mem, reg, pc))
        ;
}

//Returns true if the processed instruction is not syscall, false if otherwise.
bool step(uint8_t mem[], uint32_t reg[], int* pc)
{
   uint8_t op, rs, rt, rd, fn;
   uint16_t immediateValue;
   uint32_t instruction = getWord(mem, *pc);

   op = instruction >> 26;

   DEBUG_PRINT("PC:\t0x%.8x\nINSTR:\t0x%.8x\nOP:\t%.2u\n", *pc, instruction, op);

   switch (op)
   {
   case 0: //R type instruction
      rs = getRs(instruction);
      rt = getRt(instruction);
      rd = getRd(instruction);
      fn = getFn(instruction);
      DEBUG_PRINT("FN:\t%.2u\nRS:\t%.2u\nRT:\t%.2u\nRD:\t%.2u\n", fn, rs, rt, rd);
      switch (fn)
      {
      case 8: //jr
         DEBUG_PRINT("jr\n");
         *pc = reg[rs];
         break;
      case 12: //syscall
         DEBUG_PRINT("syscall\n");
         return false;
      default: //other r-type
         reg[rd] = alu(fn, reg[rs], reg[rt]);
         *pc += 4;
         break;
      }
      break;

   case 3: //jal
      DEBUG_PRINT("jal\n");
      reg[31] = (uint32_t)(*pc + 4);
      *pc = getJumpTargetAddr(instruction, *pc);
      break;
   case 2: //j
      DEBUG_PRINT("j\n");
      *pc = getJumpTargetAddr(instruction, *pc);
      break;

   default: //I type instruction
      rs = getRs(instruction);
      rt = getRt(instruction);
      immediateValue = getImmediateValue(instruction);
      DEBUG_PRINT("RS:\t%.2u\nRT:\t%.2u\nIMM:\t%.6d\n", rs, rt, immediateValue);
      if (op == 1 || op == 4 || op == 5)//bltz, beq, bne
      {
         branchImmediate(op, rs, rt, immediateValue, reg, pc);
      }
      else if (op == 8) //addi
      {
         DEBUG_PRINT("addi\n");
         int16_t signed_imm_val = 0xFFFF & immediateValue;
         reg[rt] = reg[rs] + signed_imm_val;
         *pc += 4;
      }
      else if (op == 12 || op == 13 || op == 14) //andi, ori, xori
      {
         aluImmediate(op, rs, rt, immediateValue, reg, pc);
      }
      else if (op == 10)//slti
      {
         DEBUG_PRINT("slti\n");
         if (reg[rs] < immediateValue)
            reg[rt] = 1;
         else
            reg[rt] = 0;
         *pc += 4;
      }
      else if (op == 15 || op == 35 || op == 43)//lui, lw, sw
      {
         lsImmediate(op, rs, rt, immediateValue, reg, mem, pc);
      }
      else //bad instruction
         fputs(ERROR_BAD_INSTR, stderr);
   }
   return true;
}


uint8_t getRs(uint32_t instruction)
{
    uint8_t rs;
    rs = instruction >> 21;
    rs = 0x0000001F & rs;
    return rs;
}

uint8_t getRt(uint32_t instruction)
{
    uint8_t rt;
    rt = instruction >> 16;
    rt = 0x0000001F & rt;
    return rt;
}

uint8_t getRd(uint32_t instruction)
{
    uint8_t rd;
    rd = instruction >> 11;
    rd = 0x0000001F & rd;
    return rd;
}

uint8_t getFn(uint32_t instruction)
{
    uint8_t fn;
    fn = 0x0000003F & instruction;
    return fn;
}

uint16_t getImmediateValue(uint32_t instruction)
{
    //Only the significant 16 bits are truncated, as specified in the C standard.
    return (uint16_t)instruction;
}

uint32_t getJumpTargetAddr(uint32_t instruction, int pc)
{
    uint32_t lower_addr = (0x03FFFFFF & instruction) << 2;
    uint32_t upper_addr = pc & 0xF0000000;
    DEBUG_PRINT("PC <-\t0x%.8x\n", lower_addr | upper_addr);
    return lower_addr | upper_addr;
}

uint32_t alu(int fn, uint32_t v1, uint32_t v2)
{
    if (fn == 32) { //add
        DEBUG_PRINT("add\n");
        return v1 + v2;
    }
    else if (fn == 34) { //sub
        DEBUG_PRINT("sub\n");
        return v1 - v2;
    }
    else if (fn == 36) { //and
        DEBUG_PRINT("and\n");
        return v1 & v2;
    }
    else if (fn == 37) { //or
        DEBUG_PRINT("or\n");
        return v1 | v2;
    }
    else if (fn == 38) { //xor
        DEBUG_PRINT("xor\n");
        return v1 ^ v2;
    }
    else if (fn == 39) { //nor
        DEBUG_PRINT("nor\n");
        return !(v1 | v2);
    }
    else if (fn == 42) { //slt
        DEBUG_PRINT("slt\n");
        if (v1 < v2)
            return 1;
        else
            return 0;
    }
    return 0;
}

void aluImmediate(int opcode,
   uint8_t rs,
   uint8_t rt,
   uint16_t immediateValue,
   uint32_t reg[],
   int *pc)
{
   if (opcode == 12) { //andi
      DEBUG_PRINT("andi\n");
      reg[rt] = reg[rs] & immediateValue;
   }
   else if (opcode == 13) { //ori
      DEBUG_PRINT("ori\n");
      reg[rt] = reg[rs] | immediateValue;
   }
   else if (opcode == 14) { //xori
      DEBUG_PRINT("xori\n");
      reg[rt] = reg[rs] ^ immediateValue;
   }
   *pc += 4;
   return;
}

void lsImmediate(int opcode,
   uint8_t rs,
   uint8_t rt,
   uint16_t immediateValue,
   uint32_t reg[],
   uint8_t mem[],
   int *pc) //Load and Store
{
   if (opcode == 15) { //lui
      DEBUG_PRINT("lui\n");
      reg[rt] = immediateValue << 16;
   }
   else if (opcode == 35) { //lw
      DEBUG_PRINT("lw\n");
      reg[rt] = getWord(mem, reg[rs] + immediateValue);
   }
   else if (opcode == 43) { //sw
      DEBUG_PRINT("sw\n");
      uint32_t *writeptr = (uint32_t*)&mem[reg[rs] + immediateValue];
      if (!((uint64_t)writeptr / 4)) {
         fputs(ERROR_BAD_MEMACCESS, stderr);
         exit(EXIT_FAILURE);
      }
      *writeptr = changeEndian(reg[rt]);
   }
   *pc += 4;
   return;
}

void branchImmediate(int opcode,
   uint8_t rs,
   uint8_t rt,
   uint16_t immediateValue,
   uint32_t reg[],
   int *pc) //Branch
{
   if (opcode == 1) { //bltz
      DEBUG_PRINT("bltz\n");
      if (reg[rs] >> 31) //If the most significant bit is 1
         *pc += 4 * immediateValue;
   }
   else if (opcode == 4) { //beq
      DEBUG_PRINT("beq\n");
      if (reg[rs] == reg[rt])
         *pc += 4 * immediateValue;
   }
   else if (opcode == 5) { //bne
      DEBUG_PRINT("bne\n");
      if (reg[rs] != reg[rt])
         *pc += 4 * immediateValue;
   }
   *pc += 4;
   return;
}

uint32_t changeEndian(uint32_t src) {
    return (src & 0x000000FF) << 24
         | (src & 0x0000FF00) << 8
         | (src & 0x00FF0000) >> 8
         | (src & 0xFF000000) >> 24;
}
